#!/usr/bin/env bash

config_hardened_modprobe() {
echo -e " 
#  ____  _  _  __    __    ____  ____ 
# (  _ \/ )( \(  )  (  )  (  __)(_  _)
#  ) _ () \/ (/ (_/\/ (_/\ ) _)   )(  
# (____/\____/\____/\____/(____) (__) 
#       xmpp:secven@bullet.li

## Copyright (C) 2012 - 2022 ENCRYPTED SUPPORT LP <adrelanos@whonix.org>
## See the file COPYING for copying conditions.

## See the following links for a community discussion and overview regarding the selections
## https://forums.whonix.org/t/blacklist-more-kernel-modules-to-reduce-attack-surface/7989
## https://madaidans-insecurities.github.io/guides/linux-hardening.html#kasr-kernel-modules

## Disable automatic conntrack helper assignment
## https://phabricator.whonix.org/T486
options nf_conntrack nf_conntrack_helper=0

## Disable bluetooth to reduce attack surface due to extended history of security vulnerabilities
## https://en.wikipedia.org/wiki/Bluetooth#History_of_security_concerns
install bluetooth /bin/true
install btusb /bin/true

## Disable thunderbolt and firewire modules to prevent some DMA attacks
install thunderbolt /bin/true
install firewire-core /bin/true
install firewire_core /bin/true
install firewire-ohci /bin/true
install firewire_ohci /bin/true
install firewire_sbp2 /bin/true
install firewire-sbp2 /bin/true
install ohci1394 /bin/true
install sbp2 /bin/true
install dv1394 /bin/true
install raw1394 /bin/true
install video1394 /bin/true

## Disable CPU MSRs as they can be abused to write to arbitrary memory.
## https://security.stackexchange.com/questions/119712/methods-root-can-use-to-elevate-itself-to-kernel-mode
install msr /bin/true

## Disables unneeded network protocols that will likely not be used as these may have unknown vulnerabilties.
## Credit to Tails (https://tails.boum.org/blueprint/blacklist_modules/) for some of these.
## > Debian ships a long list of modules for wide support of devices, filesystems, protocols. Some of these modules have a pretty bad security track record, and some of those are simply not used by most of our users.
## > Other distributions like Ubuntu[1] and Fedora[2] already ship a blacklist for various network protocols which aren't much in use by users and have a poor security track record.
install dccp /bin/true
install sctp /bin/true
install rds /bin/true
install tipc /bin/true
install n-hdlc /bin/true
install ax25 /bin/true
install netrom /bin/true
install x25 /bin/true
install rose /bin/true
install decnet /bin/true
install econet /bin/true
install af_802154 /bin/true
install ipx /bin/true
install appletalk /bin/true
install psnap /bin/true
install p8023 /bin/true
install p8022 /bin/true
install can /bin/true
install atm /bin/true

## Disable uncommon file systems to reduce attack surface
install cramfs /bin/true
install freevxfs /bin/true
install jffs2 /bin/true
install hfs /bin/true
install hfsplus /bin/true
install udf /bin/true

## Disable uncommon network file systems to reduce attack surface
install cifs /bin/true
install nfs /bin/true
install nfsv3 /bin/true
install nfsv4 /bin/true
install ksmbd /bin/true
install gfs2 /bin/true

## Disables the vivid kernel module as it's only required for testing and has been the cause of multiple vulnerabilities
## https://forums.whonix.org/t/kernel-recompilation-for-better-hardening/7598/233
## https://www.openwall.com/lists/oss-security/2019/11/02/1
## https://github.com/a13xp0p0v/kconfig-hardened-check/commit/981bd163fa19fccbc5ce5d4182e639d67e484475
install vivid /bin/true

## Disable Intel Management Engine (ME) interface with the OS
## https://www.kernel.org/doc/html/latest/driver-api/mei/mei.html
install mei /bin/true
install mei-me /bin/true

## Blacklist automatic loading of the Atheros 5K RF MACs madwifi driver
## https://git.launchpad.net/ubuntu/+source/kmod/tree/debian/modprobe.d/blacklist-ath_pci.conf?h=ubuntu/disco
blacklist ath_pci

## Blacklist automatic loading of miscellaneous modules
## https://git.launchpad.net/ubuntu/+source/kmod/tree/debian/modprobe.d/blacklist.conf?h=ubuntu/disco
blacklist evbug
blacklist usbmouse
blacklist usbkbd
blacklist eepro100
blacklist de4x5
blacklist eth1394
blacklist snd_intel8x0m
blacklist snd_aw2
blacklist prism54
blacklist bcm43xx
blacklist garmin_gps
blacklist asus_acpi
blacklist snd_pcsp
blacklist pcspkr
blacklist amd76x_edac
blacklist psmouse

## Blacklist automatic loading of framebuffer drivers
## https://git.launchpad.net/ubuntu/+source/kmod/tree/debian/modprobe.d/blacklist-framebuffer.conf?h=ubuntu/disco
blacklist aty128fb
blacklist atyfb
#blacklist radeonfb
blacklist cirrusfb
blacklist cyber2000fb
blacklist cyblafb
blacklist gx1fb
blacklist hgafb
blacklist i810fb
#blacklist intelfb
blacklist kyrofb
blacklist lxfb
blacklist matroxfb_bases
blacklist neofb
#blacklist nvidiafb
blacklist pm2fb
blacklist rivafb
blacklist s1d13xxxfb
blacklist savagefb
blacklist sisfb
blacklist sstfb
blacklist tdfxfb
blacklist tridentfb
#blacklist vesafb
blacklist vfb
blacklist viafb
blacklist vt8623fb
blacklist udlfb

## Disable CD-ROM devices
## https://nvd.nist.gov/vuln/detail/CVE-2018-11506
## https://forums.whonix.org/t/blacklist-more-kernel-modules-to-reduce-attack-surface/7989/31
#install cdrom /bin/disabled-cdrom-by-security-misc
#install sr_mod /bin/disabled-cdrom-by-security-misc
blacklist cdrom
blacklist sr_mod
" > /etc/modprobe.d/security-misc.conf
}