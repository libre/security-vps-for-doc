ARG DEVUAN=dyne/devuan:chimaera

FROM ${DEVUAN}

RUN \
    apt update && apt -y install --no-install-recommends ruby neofetch \
        figlet kmod gnupg grub gcc make openssh-server net-tools \
        iptables ipset pv cowsay jp2a \
        && gem install lolcat 
        
WORKDIR /app

COPY src /app/src

COPY secured /app/secured

RUN \
    echo 'figlet SECURED DEV | lolcat' >> $HOME/.bashrc

ENTRYPOINT [ "bash", "/app/entrypoint.sh" ]
CMD [ "" ]