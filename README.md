## 🛠️ Auto install method `🛡️ secured` for Doc

```sh
sudo sh -c 'apt update -qq && apt -y upgrade && apt -y install git'
```
```sh
git clone https://git.disroot.org/libre/security-vps-for-doc.git && cd security-vps-for-doc && chmod +x secured
```
```sh 
# Install script system

sudo ./secured --install
```
```sh
# Install linux libre and reboot system

sudo secured --libre
```
```sh
# Install base protect server vps

sudo secured --all
```
```sh
# Install docker and docker-compose

sudo secured --docker
```
```sh
# Install dante proxy

sudo secured --proxy
```
```sh
# Install WireGuard

sudo secured --wgeasy
```
```sh
# Install nextcloud

sudo secured --nextcloud
```
```sh
# Install Docker + wgeasy

sudo ./secured --docker --wgeasy
```
```sh
# List of available commands

sudo secured --help
```
```sh
# Update the script

sudo secured --update
```
```sh
# Как поставить sshto ?

sudo sh -c 'wget https://raw.githubusercontent.com/vaniacer/sshto/master/sshto && mv sshto /usr/local/bin/sshto && chmod +x /usr/local/bin/sshto && apt -y install dialog gawk'
```

### 🛠️ Создаем конфиг для удобного коннекта

```
Host *
    KexAlgorithms curve25519-sha256,curve25519-sha256@libssh.org,diffie-hellman-group16-sha512,diffie-hellman-group18-sha512
    Ciphers chacha20-poly1305@openssh.com,aes256-gcm@openssh.com,aes256-ctr
    MACs hmac-sha2-256-etm@openssh.com,hmac-sha2-512-etm@openssh.com
    HostKeyAlgorithms ssh-ed25519,ssh-ed25519-cert-v01@openssh.com,sk-ssh-ed25519@openssh.com,sk-ssh-ed25519-cert-v01@openssh.com,rsa-sha2-256,rsa-sha2-512,rsa-sha2-256-cert-v01@openssh.com,rsa-sha2-512-cert-v01@openssh.com

Host secure
    User secure
    port 1337
    HostName 195.0.0.201
    VisualHostKey no
    IdentityFile ~/.ssh/id_rsa
```

```sh
~$ nano ~/.ssh/config

~$ chmod 644 ~/.ssh/config

~$ sshto
```

```sh
# Установка openvpn пока через скрипт

~$ curl -O https://raw.githubusercontent.com/angristan/openvpn-install/master/openvpn-install.sh

~$ chmod +x openvpn-install.sh

~$ AUTO_INSTALL=y ./openvpn-install.sh
```
